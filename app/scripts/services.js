'use strict';
angular.module('IonicLift.services', [])

/**
 * A simple example service that returns some data.
 */
.factory('Friends', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var friends = [
    { id: 0, name: 'Scruff McGruff' },
    { id: 1, name: 'G.I. Joe' },
    { id: 2, name: 'Miss Frizzle' },
    { id: 3, name: 'Ash Ketchum' }
  ];

  return {
    all: function() {
      return friends;
    },
    get: function(friendId) {
      // Simple index lookup
      return friends[friendId];
    }
  };
})

.factory('GoalsService', function(){
    var goals = [
        {id: 0, name:'Exercise'},
        {id: 1, name:'Read'},
        {id: 2, name:'Take Vitamins'},
        {id: 3, name:'Brush Teeth'},
        {id: 4, name:'Make Bed'}

    ];

    return{
        getAll: function(){
            return goals;
        }
    }

});
