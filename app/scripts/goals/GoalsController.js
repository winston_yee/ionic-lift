'use strict';
angular.module('IonicLift.controllers', [])
.controller('GoalsController', function($scope, GoalsService){
    $scope.goals = GoalsService.getAll();
    $scope.done = false;
})
