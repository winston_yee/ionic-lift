angular.module('IonicLift.directives', [])

    .directive('goalDirective', function(){
        return{
            scope:true,
            restrict: 'A',
            templateUrl:'/scripts/goals/goal.html',
            link: function($scope, element){
                $scope.isDone = false;
                $scope.toggleDone = function(){
                    $scope.isDone = !$scope.isDone;
                }
            }
        }

    });